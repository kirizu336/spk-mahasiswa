<?php
namespace App\Imports;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Support\Arr;
use App\AppSetup;
use App\Kriteria;
use App\Mahasiswa;
use App\Ranking;
use App\MahasiswaHistory;

class MahasiswaImport implements OnEachRow
{
    public function onRow(Row $row)
    {
    	$mainSetup = AppSetup::first();
    	$rowIndex = $row->getIndex();
        $row = $row->toArray();

        //skip jika header atau bukan value dari data
        if ($row == null || $row == 'Nim' || $rowIndex < 2) {
    		return null;
    	}

    	if ($row[5] < 3.000 || (int)$row[6] < 8) {
    		return null;
    	}

    	// dd($value);
    	$mahasiswa = [
    		'nim' => (int)$row[0],
    		'nama_lengkap' => $row[1],
    		'jenis_kelamin' => $row[2],
    		'prodi' => $row[3],
    		'alamat_lengkap' => $row[4],
    		'ipk' => $row[5],
    		'total_sks' => (int)$row[6],
    		'total_point_tak' => (int)$row[7],
    		'penghasilan_orang_tua' => str_replace('.', '', $row[8]),
    		'semester' => (int)$row[9],
    		'angkatan' => (int)$row[10],
    		'tahun_ajaran' => $row[11]
    	];

      if ($mahasiswa['tahun_ajaran'] != $mainSetup->periode_tahun_ajaran) {
        return null;
      }

    	$status = 'belum-pernah';
    	$checkOtherData = MahasiswaHistory::where('nim_mahasiswa', $mahasiswa['nim'])
    	->where('tahun_ajaran', '<', $mainSetup->periode_tahun_ajaran)
    	->get();

    	foreach ($checkOtherData as $data) {
    		if ($data->status == 'sudah-pernah') {
    			$status = $data->status;
    		}
    	}

    	$mahasiswa['status'] = $status;

        //input kriteria
    	$nilai_c1 = 0;
    	$nilai_c2 = 0;
    	$nilai_c3 = 0;
    	$nilai_c4 = 0;

		//rule1
		$c1 = $mahasiswa['ipk'];
		if (4.000 == $c1 ) {
	        $nilai_c1 = 100;
	    } elseif (3.999 >= $c1 && $c1 >= 3.875) {
	        $nilai_c1 = 95;
	    } elseif (3.874 >= $c1 && $c1 >= 3.750) {
	        $nilai_c1 = 90;
	    } elseif (3.749 >= $c1 && $c1 >= 3.625) {
	        $nilai_c1 = 85;
	    } elseif (3.624 >= $c1 && $c1 >= 3.500) {
	        $nilai_c1 = 80;
	    } elseif (3.499 >= $c1 && $c1 >= 3.375) {
	        $nilai_c1 = 75;
	    } elseif (3.374 >= $c1 && $c1 >= 3.250) {
	        $nilai_c1 = 70;
	    } elseif (3.249 >= $c1 && $c1 >= 3.125) {
	        $nilai_c1 = 65;
	    } elseif (3.124 >= $c1 && $c1 >= 3.000) {
	        $nilai_c1 = 60;
	    }else{
	    	return null;
	    }


		//rule2
		$c2 = round($mahasiswa['total_sks'] / $mahasiswa['semester']);
		if ($c2 >= 24 ) {
	        $nilai_c2 = 100;
	    } elseif (23 >= $c2 && $c2 >= 22) {
	        $nilai_c2 = 95;
	    } elseif (21 >= $c2 && $c2 >= 20) {
	        $nilai_c2 = 90;
	    } elseif (19 >= $c2 && $c2 >= 18) {
	        $nilai_c2 = 85;
	    } elseif (17 >= $c2 && $c2 >= 16) {
	        $nilai_c2 = 80;
	    } elseif (15 >= $c2 && $c2 >= 14) {
	        $nilai_c2 = 75;
	    } elseif (13 >= $c2 && $c2 >= 12) {
	        $nilai_c2 = 70;
	    } elseif (11 >= $c2 && $c2 >= 10) {
	        $nilai_c2 = 65;
	    } elseif (9 >= $c2 && $c2 >= 8) {
	    	$nilai_c2 = 60;
	    }else{
	    	return null;
	    }


		//rule3
		$c3 = round($mahasiswa['total_point_tak'] / $mahasiswa['semester']);
		if ($c3 >= 16 ) {
      		$nilai_c3 = 100;
      	} elseif (15 >= $c3 && $c3 >= 14) {
        	$nilai_c3 = 95;
      	} elseif (13 >= $c3 && $c3 >= 12) {
        	$nilai_c3 = 90;
      	} elseif (11 >= $c3 && $c3 >= 10) {
        	$nilai_c3 = 85;
     	} elseif (9 >= $c3 && $c3 >= 8) {
        	$nilai_c3 = 80;
      	} elseif (7 >= $c3 && $c3 >= 6) {
        	$nilai_c3 = 75;
      	} elseif (5 >= $c3 && $c3 >= 4) {
        	$nilai_c3 = 70;
      	} elseif (3 >= $c3 && $c3 >= 2) {
        	$nilai_c3 = 65;
      	} elseif (1 >= $c3 && $c3 >= 0) {
        	$nilai_c3 = 60;
      	}else{
      		return null;
      	}

		//rule4
		$c4 = $mahasiswa['penghasilan_orang_tua'];
		if ($c4 < 1000000 ) {
        	$nilai_c4 = 100;
      	} elseif (2000000 >= $c4 && $c4 >= 1000000) {
        	$nilai_c4 = 90;
      	} elseif (3000000 >= $c4 && $c4 > 2000000) {
        	$nilai_c4 = 80;
      	} elseif (4000000 >= $c4 && $c4 > 3000000) {
        	$nilai_c4 = 70;
      	}elseif ($c4 > 4000000) {
        	$nilai_c4 = 60;
      	}

		$kriteria = [
			'nim_mahasiswa' => $mahasiswa['nim'],
			'nilai_c1' => $nilai_c1,
			'nilai_c2' => $nilai_c2,
			'nilai_c3' => $nilai_c3,
			'nilai_c4' => $nilai_c4,
			'tahun_ajaran' => $mahasiswa['tahun_ajaran']
		];

    $mahasiswaMainData = Arr::except($mahasiswa, ['ipk', 'total_sks', 'total_point_tak', 'semester', 'tahun_ajaran', 'status', 'penghasilan_orang_tua']);

    $mahasiswaHistoryData = Arr::except($mahasiswa, ['nim', 'nama_lengkap', 'jenis_kelamin', 'prodi', 'alamat_lengkap', 'angkatan']);
    $mahasiswaHistoryData['nim_mahasiswa'] = $mahasiswa['nim'];

		
		$deleteSameKriteria = Kriteria::where('nim_mahasiswa', $mahasiswa['nim'])
		->where('tahun_ajaran', $mahasiswa['tahun_ajaran'])
		->delete();
    $deleteSameHistory = MahasiswaHistory::where('nim_mahasiswa', $mahasiswa['nim'])
    ->where('tahun_ajaran', $mahasiswa['tahun_ajaran'])
    ->delete();
    $deleteSameMhs = Mahasiswa::where('nim', $mahasiswa['nim'])
    ->delete();

		$createMhs = Mahasiswa::create($mahasiswaMainData);
    $createHistory = MahasiswaHistory::create($mahasiswaHistoryData);
		$createKriteria = Kriteria::create($kriteria);
  }
}