<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSetup extends Model
{
    protected $table = 'app_setup';
    protected $fillable = [
    	'periode_tahun_ajaran', 'kuota_beasiswa', 'bobot_c1', 'bobot_c2', 'bobot_c3', 'bobot_c4'
    ];
}
