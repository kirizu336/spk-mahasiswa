<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MahasiswaHistory extends Model
{
    protected $table = 'mahasiswa_history';
    protected $fillable = [
    	'nim_mahasiswa', 'ipk', 'total_sks', 'total_point_tak', 'semester', 'tahun_ajaran', 'status', 'penghasilan_orang_tua'
    ];

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa', 'nim_mahasiswa', 'nim');
    }
}
