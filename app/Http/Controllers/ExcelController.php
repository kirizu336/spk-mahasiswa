<?php

namespace App\Http\Controllers;

use App\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Kriteria;

class ExcelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function importMahasiswa()
	{
		return view('pages.mahasiswa.import');
	}

    public function import(Request $request) 
    {
    	$validatedData = $request->validate([
    		'file' => 'required|mimes:xls,xlsx,csv'
    	]);

    	$import = Excel::import(new MahasiswaImport, $validatedData['file']);
        
        return redirect()->to(route('mahasiswa.index'));
    }
}
