<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppSetup;
use App\Kriteria;
use App\Ranking;
use App\Mahasiswa;

class SeleksiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function main(){
    	$currAppSetting = AppSetup::first();

    	return view('pages.seleksi-beasiswa.index')
    	->with('currAppSetting', $currAppSetting);
    }

    public function spk(){
    	$mainSetup = AppSetup::first();
    	$kriteriaMhs = Kriteria::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
    	->with(['mahasiswa' => function($q) use ($mainSetup) {
            $q->whereHas('history', function($q2) use ($mainSetup) {
                $q2->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
            });
        }])
    	->get();

        if (count($kriteriaMhs) > 0) {
            $bobot_c1 = $mainSetup->bobot_c1;
            $bobot_c2 = $mainSetup->bobot_c2;
            $bobot_c3 = $mainSetup->bobot_c3;
            $bobot_c4 = $mainSetup->bobot_c4;

            //perbaikan bobot
            $c1 = round($bobot_c1/($bobot_c1+$bobot_c2+$bobot_c3+$bobot_c4),2);
            $c2 = round($bobot_c2/($bobot_c1+$bobot_c2+$bobot_c3+$bobot_c4),2);
            $c3 = round($bobot_c3/($bobot_c1+$bobot_c2+$bobot_c3+$bobot_c4),2);
            $c4 = round($bobot_c4/($bobot_c1+$bobot_c2+$bobot_c3+$bobot_c4),2);

            //hasil skor
            foreach ($kriteriaMhs as $kriteria) {
                $kriteria->c1_pow = round(pow((float)$kriteria->nilai_c1,$c1),3);
                $kriteria->c2_pow = round(pow($kriteria->nilai_c2,$c2),3);
                $kriteria->c3_pow = round(pow($kriteria->nilai_c3,$c3),3);
                $kriteria->c4_pow = round(pow($kriteria->nilai_c4,$c4),3);
                $kriteria->total_pow = round((pow($kriteria->nilai_c1,$c1)*pow($kriteria->nilai_c2,$c2)*pow($kriteria->nilai_c3,$c3)*pow($kriteria->nilai_c4,$c4)),10);
            }

            //hasil vektor
            $grandTotalPow = 0;
            foreach ($kriteriaMhs as $kriteria) {
                $grandTotalPow = $grandTotalPow + $kriteria->total_pow;
            };

            // dd($debug);

            foreach ($kriteriaMhs as $mahasiswa) {
                $rankedMhs[] = [
                    'nim_mahasiswa' => $mahasiswa->mahasiswa->nim,
                    'c1_pow' => $mahasiswa->c1_pow,
                    'c2_pow' => $mahasiswa->c2_pow,
                    'c3_pow' => $mahasiswa->c3_pow,
                    'c4_pow' => $mahasiswa->c4_pow,
                    'total_pow' => round($mahasiswa->total_pow / $grandTotalPow, 10),
                    'tahun_ajaran' => $mainSetup->periode_tahun_ajaran
                ];
            };

            $deleteCurrRanking = Ranking::select('id')
            ->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
            ->delete();

            //finally insert to db
            $rankedInsert = Ranking::insert($rankedMhs);

            //get final result
            $finalResult = Ranking::with(['mahasiswa' => function($q) use ($mainSetup) {
                $q->whereHas('history', function($q2) use ($mainSetup) {
                    $q2->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
                });
            }])
            ->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
            ->get();

            return view('pages.seleksi-beasiswa.index')
            ->with('currAppSetting', $mainSetup)
            ->with('c1', $c1)
            ->with('c2', $c2)
            ->with('c3', $c3)
            ->with('c4', $c4)
            ->with('hasilSkor', $kriteriaMhs)
            ->with('finalResult', $finalResult);
        }else{
            return redirect()->back()
            ->withErrors(['error' => 'Tidak ditemukan data mahasiswa pada periode tahun ajaran '.$mainSetup->periode_tahun_ajaran]);
        }
    }
}
