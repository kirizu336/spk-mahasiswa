<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppSetup;
use App\Ranking;
use App\MahasiswaHistory;

class AppSetupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function indexAppSetup()
    {
    	$currSetup = AppSetup::first();
    	return view('pages.settings.index')
    	->with('currSetup', $currSetup);
    }

    public function updateSetting(Request $request)
    {
    	$validatedData = $request->validate([
    		'periode_tahun_ajaran_part_one' => 'required',
    		'periode_tahun_ajaran_part_two' => 'required',
            'kuota_beasiswa' => 'required',
    		'bobot_c1' => 'required',
    		'bobot_c2' => 'required',
    		'bobot_c3' => 'required',
    		'bobot_c4' => 'required'
    	]);

    	$validatedData['periode_tahun_ajaran'] = $validatedData['periode_tahun_ajaran_part_one'].'/'.$validatedData['periode_tahun_ajaran_part_two'];
    	unset($validatedData['periode_tahun_ajaran_part_one']);
    	unset($validatedData['periode_tahun_ajaran_part_two']);

    	$currSettings = AppSetup::first()->update($validatedData);

    	if ($currSettings) {
            $rankings = Ranking::where('tahun_ajaran', $validatedData['periode_tahun_ajaran'])
            ->get();

            if (count($rankings) > 0) {
                foreach ($rankings as $ranking) {
                    $status = 'belum-pernah';
                    $checkOtherData = MahasiswaHistory::where('nim_mahasiswa', $ranking->nim_mahasiswa)
                    ->where('tahun_ajaran', '<', $validatedData['periode_tahun_ajaran'])
                    ->get();

                    foreach ($checkOtherData as $data) {
                        if ($data->status == 'sudah-pernah') {
                            $status = $data->status;
                        }
                    }

                    if ($status == 'sudah-pernah') {
                        $updateStatus = MahasiswaHistory::where('nim_mahasiswa', $ranking->nim_mahasiswa)
                        ->where('tahun_ajaran', $validatedData['periode_tahun_ajaran'])
                        ->update(['status' => 'sudah-pernah']);
                    }
                }
            }

    		return redirect()->back();
    	}else{
    		return redirect()->back()
    		->withErrors(['error' => 'terjadi kesalahan silahkan hubungi pihak administrator!']);
    	}
    }
}
