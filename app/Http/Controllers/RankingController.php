<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Ranking;
use App\AppSetup;
use App\Mahasiswa;
use App\MahasiswaHistory;
use Auth;
use PDF;

class RankingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexRanking()
    {
    	return view('pages.ranking.index');
    }

    public function ajaxRanking(Request $request)
    {
        $mainSetup = AppSetup::first();
        $rankings = Ranking::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
        ->with(['mahasiswa', 'mahasiswa.firstHistory' => function($q) use ($mainSetup) {
            $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
        }])
        ->join('mahasiswa', 'mahasiswa.nim', '=', 'rankings.nim_mahasiswa')
        ->orderBy('total_pow', 'desc')
        ->orderBy('mahasiswa.angkatan')
        ->get();

        foreach ($rankings as $ranking) {
            $ranking->status = $ranking->mahasiswa->firstHistory->first()->status;
        }

        if ($request->has('status') && !is_null($request->status) && $request->status !== 'all') {
        	return datatables()->of($rankings)
            ->filter(function($instance) use ($request){
                $instance->collection = $instance->collection
                ->filter(function ($row) use ($request) {
                    return \Str::contains($row['status'], $request->status) ? true : false;
                });
            })
            ->addColumn('action', function($ranking) {
                if (Auth::user()->role_id === 1) {
                    return "<button class=\"btn btn-primary details-control\">Detail</button>";
                }else{
                     return "<a href=".route('ranking.edit', ['id' => $ranking->id])." class=\"btn btn-info\">Edit Status</a> <button class=\"btn btn-primary details-control\">Detail</button>";
                }
            })
            ->editColumn('status', function($ranking){
                if ($ranking->status === 'sudah-pernah') {
                    return "<div class=\"badge badge-danger\">Sudah Pernah</div>";
                }else{
                    return "<div class=\"badge badge-success\">Belum Pernah</div>";
                }
            })
            ->escapeColumns('')
            ->make(true);
        }else{
            return datatables()->of($rankings)
            ->addColumn('action', function($ranking) {
                if (Auth::user()->role_id === 1) {
                    return "<button class=\"btn btn-primary details-control\">Detail</button>";
                }else{
                     return "<a href=".route('ranking.edit', ['id' => $ranking->id])." class=\"btn btn-info\">Edit Status</a> <button class=\"btn btn-primary details-control\">Detail</button>";
                }
            })
            ->editColumn('status', function($ranking){
                if ($ranking->status === 'sudah-pernah') {
                    return "<div class=\"badge badge-danger\">Sudah Pernah</div>";
                }else{
                    return "<div class=\"badge badge-success\">Belum Pernah</div>";
                }
            })
            ->escapeColumns('')
            ->make(true);
        }
    }

    public function cetakRanking(Request $request)
    {
        $mainSetup = AppSetup::first();

        if ($request->has('status') && !is_null($request->status) && $request->status !== 'all') {

            if ($mainSetup->kuota_beasiswa > 0) {
                $rankings = Ranking::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
                ->whereHas('mahasiswa.firstHistory', function($query) use($request) {
                    $query->where('status', $request->status);
                })
                ->with(['mahasiswa','mahasiswa.firstHistory' => function($q) use ($mainSetup) {
                    $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
                }])
                ->join('mahasiswa', 'mahasiswa.nim', '=', 'rankings.nim_mahasiswa')
                ->orderBy('total_pow', 'desc')
                ->orderBy('mahasiswa.angkatan')
                ->take($mainSetup->kuota_beasiswa)
                ->get();
            }else{
                $rankings = Ranking::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
                ->whereHas('mahasiswa.firstHistory', function($query) use($request){
                    $query->where('status', $request->status);
                })
                ->with(['mahasiswa','mahasiswa.firstHistory' => function($q) use ($mainSetup) {
                    $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
                }])
                ->join('mahasiswa', 'mahasiswa.nim', '=', 'rankings.nim_mahasiswa')
                ->orderBy('total_pow', 'desc')
                ->orderBy('mahasiswa.angkatan')
                ->get();
            }
            
        }else{

            if ($mainSetup->kuota_beasiswa > 0) {
                $rankings = Ranking::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
                ->with(['mahasiswa','mahasiswa.firstHistory' => function($q) use ($mainSetup) {
                    $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
                }])
                ->join('mahasiswa', 'mahasiswa.nim', '=', 'rankings.nim_mahasiswa')
                ->orderBy('total_pow', 'desc')
                ->orderBy('mahasiswa.angkatan')
                ->take($mainSetup->kuota_beasiswa)
                ->get();
            }else{
                $rankings = Ranking::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
                ->with(['mahasiswa','mahasiswa.firstHistory' => function($q) use ($mainSetup) {
                    $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
                }])
                ->join('mahasiswa', 'mahasiswa.nim', '=', 'rankings.nim_mahasiswa')
                ->orderBy('total_pow', 'desc')
                ->orderBy('mahasiswa.angkatan')
                ->get();
            }
        }

        foreach ($rankings as $ranking) {
            $updateStatus = MahasiswaHistory::where('nim_mahasiswa', $ranking->nim_mahasiswa)
            ->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
            ->update(['status' => 'sudah-pernah']);
        }

        $pdf = PDF::loadview('pages.ranking.cetak',['rankings'=>$rankings, 'setup'=>$mainSetup])
        ->setPaper('a4', 'landscape');

        return $pdf->stream();
    }

    public function cetakAll()
    {
        $mainSetup = AppSetup::first();

        $rankings = Ranking::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
        ->with(['mahasiswa','mahasiswa.firstHistory' => function($q) use ($mainSetup) {
            $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
        }])
        ->join('mahasiswa', 'mahasiswa.nim', '=', 'rankings.nim_mahasiswa')
        ->orderBy('total_pow', 'desc')
        ->orderBy('mahasiswa.angkatan')
        ->get();

        $pdf = PDF::loadview('pages.ranking.cetak',['rankings'=>$rankings, 'setup'=>$mainSetup])
        ->setPaper('a4', 'landscape');

        return $pdf->stream();
    }

    public function editRanking($id)
    {
        $mainSetup = AppSetup::first();

        $ranking = Ranking::where('id', $id)
        ->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
        ->with(['mahasiswa', 'mahasiswa.firstHistory' => function($q) use ($mainSetup) {
            $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
        }])
        ->first();

        return view('pages.ranking.edit')
        ->with('ranking', $ranking);
    }

    public function storeRanking($id, Request $request)
    {
        $mainSetup = AppSetup::first();

        $validatedData = $request->validate([
            'status' => 'required'
        ]);

        $currRanking = Ranking::select('nim_mahasiswa')->where('id', $id)->first();
        $updateRanking = MahasiswaHistory::where('nim_mahasiswa', $currRanking->nim_mahasiswa)
        ->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
        ->update(['status' => $validatedData['status']]);

        if ($updateRanking) {
            $otherStatus = false;

            $checkOtherData = MahasiswaHistory::where('nim_mahasiswa', $currRanking->nim_mahasiswa)
            ->where('tahun_ajaran', '<', $mainSetup->periode_tahun_ajaran)
            ->get();

            if(count($checkOtherData) > 0){
                foreach ($checkOtherData as $data) {
                    if ($data->status != $validatedData['status']) {
                        $otherStatus = true;
                    }
                }
            }

            if ($otherStatus) {
                $updateOtherRanking = MahasiswaHistory::where('nim_mahasiswa', $currRanking->nim_mahasiswa)
                ->where('tahun_ajaran', '<', $mainSetup->periode_tahun_ajaran)
                ->update(['status' => $validatedData['status']]);
            }

            return redirect()->to(route('ranking.index'));
        }else{
            return redirect()->back()
            ->withInput($request->input())
            ->withErrors(['error' => 'Terjadi masalah silahkan hubungi pihak administrator!']);
        }
    }
}