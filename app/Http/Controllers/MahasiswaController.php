<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Yajra\DataTables\DataTables;
use App\Mahasiswa;
use App\Kriteria;
use App\AppSetup;
use App\Ranking;
use App\MahasiswaHistory;

class MahasiswaController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function datatableIndexMahasiswa()
	{
		$mahasiswa = MahasiswaHistory::with('mahasiswa')
		->where('tahun_ajaran', AppSetup::first()->periode_tahun_ajaran)
		->get();

		return datatables()->of($mahasiswa)
        ->addColumn('action', function($mahasiswa) {
            return "<input type=\"hidden\" name=\"nim\" value=".$mahasiswa->nim_mahasiswa." /> <button class=\"btn btn-primary details-control\">Detail</button> <a href=".route('mahasiswa.edit', ['nim' => $mahasiswa->nim_mahasiswa])." class=\"btn btn-info\">Edit</a> <button class=\"btn btn-danger btn-delete\" data-remote=".route('mahasiswa.delete', ['nim' => $mahasiswa->nim_mahasiswa]).">Delete</button>";
        })
        ->editColumn('mahasiswa.jenis_kelamin', function($mahasiswa){
        	return $mahasiswa->mahasiswa->jenis_kelamin === 'L' ? 'Laki-Laki' : 'Perempuan';
        })
        ->editColumn('penghasilan_orang_tua', function($mahasiswa){
        	return number_format($mahasiswa->penghasilan_orang_tua,0);
        })
        ->make(true);
	}

	public function indexMahasiswa()
	{
		return view('pages.mahasiswa.index');
	}

	public function createMahasiswa()
	{
		return view('pages.mahasiswa.add');
	}

	public function editMahasiswa($nim)
	{
		$mainSetup = AppSetup::first();

		$currMhs = Mahasiswa::with(['history' => function($q) use($mainSetup) {
			$q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
		}])
		->where('nim', $nim)
		->first();

		if (!is_null($currMhs)) {
			$currMhs->penghasilan_orang_tua = number_format($currMhs->penghasilan_orang_tua);
			$currMhsTahunAjaran = explode('/', $currMhs->history()->first()->tahun_ajaran);

			return view('pages.mahasiswa.edit')
			->with('currMhs', $currMhs)
			->with('currMhsTahunAjaran', $currMhsTahunAjaran);
		}else{
			return redirect()->back()
			->withErrors(['error' => 'Nim tidak ditemukan!']);
		}
	}

    public function storeMahasiswa(Request $request)
    {
    	$mainSetup = AppSetup::first();
		$validatedData = $request->validate([
    		'nim' => ['required', 'numeric'],
    		'nama_lengkap' => 'required',
    		'jenis_kelamin' => 'required',
    		'prodi' => 'required',
    		'alamat_lengkap' => 'required',
    		'ipk' => ['required', 'numeric'],
    		'total_sks' => ['required', 'numeric'],
    		'total_point_tak' => ['required', 'numeric'],
    		'penghasilan_orang_tua' => ['required'],
    		'semester' => ['required', 'numeric'],
    		'angkatan' => ['required', 'numeric'],
    		'tahun_ajaran_part_one' => ['required', 'numeric'],
    		'tahun_ajaran_part_two' => ['required', 'numeric']
    	]);

    	$dapatBeasiswa = true;

    	//normalize penghasilan orang tua
    	$validatedData['penghasilan_orang_tua'] = str_replace(',', '', $validatedData['penghasilan_orang_tua']);
    	// dd($validatedData);

    	//get tahun ajaran
    	$validatedData['tahun_ajaran'] = $validatedData['tahun_ajaran_part_one'].'/'.$validatedData['tahun_ajaran_part_two'];
    	// dd($validatedData);
    	
    	unset($validatedData['tahun_ajaran_part_one']);
    	unset($validatedData['tahun_ajaran_part_two']);
    	// dd($validatedData);

    	//check inputan thn ajaran
    	if ($validatedData['tahun_ajaran'] != $mainSetup->periode_tahun_ajaran) {
    		$msg = 'Data mahasiswa bukan merupakan data tahun ajaran '.$mainSetup->periode_tahun_ajaran.'!';
    		return redirect()->back()
    		->withInput($request->input())
    		->withErrors(['error' => $msg]);
    	}

    	//check status
    	$status = 'belum-pernah';
    	$checkOtherData = MahasiswaHistory::where('nim_mahasiswa', $validatedData['nim'])
    	->where('tahun_ajaran', '<', $mainSetup->periode_tahun_ajaran)
    	->get();

    	foreach ($checkOtherData as $data) {
    		if ($data->status == 'sudah-pernah') {
    			$status = $data->status;
    		}
    	}

    	$validatedData['status'] = $status;

    	if ($validatedData['ipk'] < 3.000 || (int)$validatedData['total_sks'] < 8) {
    		return redirect()->back()
    		->withInput($request->input())
    		->withErrors(['error' => 'Nilai IPK minimal 3 dan minimal telah memiliki 8 total sks!']);
    	}

		//input kriteria
    	$nilai_c1 = 0;
    	$nilai_c2 = 0;
    	$nilai_c3 = 0;
    	$nilai_c4 = 0;

		//rule1
		$c1 = $validatedData['ipk'];
		if (4.000 == $c1 ) {
	        $nilai_c1 = 100;
	    } elseif (3.999 >= $c1 && $c1 >= 3.875) {
	        $nilai_c1 = 95;
	    } elseif (3.874 >= $c1 && $c1 >= 3.750) {
	        $nilai_c1 = 90;
	    } elseif (3.749 >= $c1 && $c1 >= 3.625) {
	        $nilai_c1 = 85;
	    } elseif (3.624 >= $c1 && $c1 >= 3.500) {
	        $nilai_c1 = 80;
	    } elseif (3.499 >= $c1 && $c1 >= 3.375) {
	        $nilai_c1 = 75;
	    } elseif (3.374 >= $c1 && $c1 >= 3.250) {
	        $nilai_c1 = 70;
	    } elseif (3.249 >= $c1 && $c1 >= 3.125) {
	        $nilai_c1 = 65;
	    } elseif (3.124 >= $c1 && $c1 >= 3.000) {
	        $nilai_c1 = 60;
	    }else{
	    	return redirect()->back()
	    	->withInput($request->input())
	    	->withErrors(['error' => 'IPK Terlalu kecil!']);
	    }


		//rule2
		$c2 = round($validatedData['total_sks'] / $validatedData['semester']);
		if ($c2 >= 24 ) {
	        $nilai_c2 = 100;
	    } elseif (23 >= $c2 && $c2 >= 22) {
	        $nilai_c2 = 95;
	    } elseif (21 >= $c2 && $c2 >= 20) {
	        $nilai_c2 = 90;
	    } elseif (19 >= $c2 && $c2 >= 18) {
	        $nilai_c2 = 85;
	    } elseif (17 >= $c2 && $c2 >= 16) {
	        $nilai_c2 = 80;
	    } elseif (15 >= $c2 && $c2 >= 14) {
	        $nilai_c2 = 75;
	    } elseif (13 >= $c2 && $c2 >= 12) {
	        $nilai_c2 = 70;
	    } elseif (11 >= $c2 && $c2 >= 10) {
	        $nilai_c2 = 65;
	    } elseif (9 >= $c2 && $c2 >= 8) {
	    	$nilai_c2 = 60;
	    }else{
	    	return redirect()->back()
	    	->withInput($request->input())
	    	->withErrors(['error' => 'Mohon maaf jumlah SKS terlalu kecil untuk mendapatkan beasiswa semester '.$validatedData['semester']]);
	    }


		//rule3
		$c3 = round($validatedData['total_point_tak'] / $validatedData['semester']);
		if ($c3 >= 16 ) {
      		$nilai_c3 = 100;
      	} elseif (15 >= $c3 && $c3 >= 14) {
        	$nilai_c3 = 95;
      	} elseif (13 >= $c3 && $c3 >= 12) {
        	$nilai_c3 = 90;
      	} elseif (11 >= $c3 && $c3 >= 10) {
        	$nilai_c3 = 85;
     	} elseif (9 >= $c3 && $c3 >= 8) {
        	$nilai_c3 = 80;
      	} elseif (7 >= $c3 && $c3 >= 6) {
        	$nilai_c3 = 75;
      	} elseif (5 >= $c3 && $c3 >= 4) {
        	$nilai_c3 = 70;
      	} elseif (3 >= $c3 && $c3 >= 2) {
        	$nilai_c3 = 65;
      	} elseif (1 >= $c3 && $c3 >= 0) {
        	$nilai_c3 = 60;
      	}else{
	    	return redirect()->back()
	    	->withInput($request->input())
	    	->withErrors(['error' => 'Mohon maaf jumlah TAK terlalu kecil untuk mendapatkan beasiswa semester '.$validatedData['semester']]);
	    }

		//rule4
		$c4 = $validatedData['penghasilan_orang_tua'];
		if ($c4 < 1000000 ) {
        	$nilai_c4 = 100;
      	} elseif (2000000 >= $c4 && $c4 >= 1000000) {
        	$nilai_c4 = 90;
      	} elseif (3000000 >= $c4 && $c4 > 2000000) {
        	$nilai_c4 = 80;
      	} elseif (4000000 >= $c4 && $c4 > 3000000) {
        	$nilai_c4 = 70;
      	}elseif ($c4 > 4000000) {
        	$nilai_c4 = 60;
      	}

      	$mahasiswaMainData = Arr::except($validatedData, ['ipk', 'total_sks', 'total_point_tak', 'semester', 'tahun_ajaran', 'status', 'penghasilan_orang_tua']);

      	$mahasiswaHistoryData = Arr::except($validatedData, ['nim', 'nama_lengkap', 'jenis_kelamin', 'prodi', 'alamat_lengkap', 'angkatan']);
      	$mahasiswaHistoryData['nim_mahasiswa'] = $validatedData['nim'];

		$checkMhs = Mahasiswa::where('nim', $validatedData['nim'])
		->count();

		if ($checkMhs > 0) {
			$currentMhs = Mahasiswa::where('nim', $validatedData['nim'])
			->update($mahasiswaMainData);
		}else{
			$currentMhs = Mahasiswa::create($mahasiswaMainData);
		}

		if (!is_null($currentMhs) || $currentMhs > 0) {
			$checkHistory = MahasiswaHistory::where('nim_mahasiswa', $validatedData['nim'])
			->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
			->count();

			if ($checkHistory > 0) {
				$currHistory = MahasiswaHistory::where('nim_mahasiswa', $validatedData['nim'])
				->where('tahun_ajaran', $validatedData['tahun_ajaran'])
				->update($mahasiswaHistoryData);
			}else{
				$currHistory = MahasiswaHistory::create($mahasiswaHistoryData);
			}
		}

		if (!is_null($currentMhs) || $currentMhs > 0) {
			$checkKriteria = Kriteria::where('nim_mahasiswa', $validatedData['nim'])
			->where('tahun_ajaran', $validatedData['tahun_ajaran'])
			->count();

			if ($checkKriteria > 0) {
				$currentKriteria = Kriteria::where('nim_mahasiswa', $validatedData['nim'])
				->where('tahun_ajaran', $validatedData['tahun_ajaran'])
				->update([
					'nilai_c1' => $nilai_c1,
					'nilai_c2' => $nilai_c2,
					'nilai_c3' => $nilai_c3,
					'nilai_c4' => $nilai_c4
				]);
			}else{
				$currentKriteria = Kriteria::create([
					'nim_mahasiswa' => $validatedData['nim'],
					'nilai_c1' => $nilai_c1,
					'nilai_c2' => $nilai_c2,
					'nilai_c3' => $nilai_c3,
					'nilai_c4' => $nilai_c4,
					'tahun_ajaran' => $validatedData['tahun_ajaran']
				]);
			}
		}
		return redirect()->to(route('mahasiswa.index'));
    }

    public function deleteMahasiswa($nim)
    {
    	$mainSetup = AppSetup::first();
    	$currMhs = Mahasiswa::where('nim', $nim)
    	->first();
    	if (!is_null($currMhs)) {
    		$currKriteria = Kriteria::where('nim_mahasiswa', $nim)
    		->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
    		->delete();
    		$currRanking = Ranking::where('nim_mahasiswa', $nim)
    		->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
    		->delete();
    		$currHistory = MahasiswaHistory::where('nim_mahasiswa', $nim)
		    ->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
		    ->delete();
    		$currMhs->delete();

    		return response()->json(['message' => 'success'], 201);
    	}else{
    		return response()->json(['message' => 'failed'], 500);
    	}
    }

    public function deleteAllMahasiswaByTahunAjaran()
    {
    	try {
    		$mainSetup = AppSetup::first();

    		Kriteria::where('tahun_ajaran',  $mainSetup->periode_tahun_ajaran)->delete();
    		Ranking::where('tahun_ajaran',  $mainSetup->periode_tahun_ajaran)->delete();
    		MahasiswaHistory::where('tahun_ajaran',  $mainSetup->periode_tahun_ajaran)
    		->delete();

    		return response()->json(['message' => 'success'], 201);	
    	} catch (\Exception $e) {
    		return response()->json(['message' => $e->getMessage()], 500);
    	}
    }
}
