<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Kriteria;
use App\AppSetup;

class KriteriaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function indexKriteria()
    {
    	return view('pages.kriteria.index');
    }

    public function ajaxKriteria()
    {
        $mainSetup = AppSetup::first();
    	$kriteriaMhs = Kriteria::where('tahun_ajaran', $mainSetup->periode_tahun_ajaran)
    	->with(['mahasiswa', 'mahasiswa.firstHistory' => function($q) use ($mainSetup) {
            $q->where('tahun_ajaran', $mainSetup->periode_tahun_ajaran);
        }])
        ->get();

    	return datatables()->of($kriteriaMhs)
        ->addColumn('action', function($kriteriaMhs) {
            return "<button class=\"btn btn-primary details-control\">Detail</button>";
        })
        ->make(true);
    }
}
