<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
	protected $table = 'nilai_alter_mhs';
    protected $fillable = [
    	'nim_mahasiswa', 'nilai_c1', 'nilai_c2', 'nilai_c3', 'nilai_c4', 'tahun_ajaran'
    ];
    protected $primaryKey = 'id';

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa', 'nim_mahasiswa', 'nim');
    }
}
