<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $fillable = [
    	'nim', 'nama_lengkap', 'jenis_kelamin', 'prodi', 'alamat_lengkap', 'angkatan'
    ];
    
    public $incrementing = false;
    public $primaryKey = 'nim';

    public function history() {
    	return $this->hasMany('App\MahasiswaHistory', 'nim_mahasiswa', 'nim');
    }

    public function firstHistory() {
    	return $this->hasMany('App\MahasiswaHistory', 'nim_mahasiswa', 'nim')->latest();
    }
}
