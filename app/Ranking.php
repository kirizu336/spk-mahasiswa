<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
    protected $table = 'rankings';
    protected $fillable = [
    	'nim_mahasiswa', 'c1_pow', 'c2_pow', 'c3_pow', 'c4_pow', 'total_pow', 'tahun_ajaran'
    ];

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa', 'nim_mahasiswa', 'nim');
    }
}
