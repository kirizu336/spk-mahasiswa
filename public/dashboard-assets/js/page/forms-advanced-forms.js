"use strict";
document.addEventListener('DOMContentLoaded', () => {
  $('.yearpicker').yearpicker();
  var cleaveC = new Cleave('.currency', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand'
  });
});