<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAppSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $yearBefore = new DateTime('-1 year');
        Schema::create('app_setup', function (Blueprint $table) use($yearBefore) {
            $table->id();
            $table->string('periode_tahun_ajaran')->default($yearBefore->format('Y').'/'.date('Y').'');
            $table->string('kuota_beasiswa')->default(10);
            $table->string('bobot_c1')->default(40);
            $table->string('bobot_c2')->default(30);
            $table->string('bobot_c3')->default(20);
            $table->string('bobot_c4')->default(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_setup');
    }
}
