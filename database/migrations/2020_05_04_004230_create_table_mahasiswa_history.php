<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMahasiswaHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_history', function (Blueprint $table) {
            $table->id();
            $table->string('nim_mahasiswa');
            $table->string('ipk');
            $table->string('total_sks');
            $table->string('total_point_tak');
            $table->string('penghasilan_orang_tua');
            $table->string('tahun_ajaran');
            $table->string('semester');
            $table->string('status')->default('belum-pernah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_history');
    }
}
