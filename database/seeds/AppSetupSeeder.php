<?php

use Illuminate\Database\Seeder;
use App\AppSetup;

class AppSetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$yearBefore = new DateTime('-1 year');
        AppSetup::create([
        	'periode_tahun_ajaran' => $yearBefore->format('Y').'/'.date('Y'),
            'bobot_c1' => 40,
            'bobot_c2' => 30,
            'bobot_c3' => 20,
            'bobot_c4' => 10
        ]);
    }
}
