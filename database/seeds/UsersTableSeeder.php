<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'role_name' => 'Staff'
            ],
            [
                'role_name' => 'Koordinator'
            ],
            [
                'role_name' => 'Admin'
            ]
        ];

        $fields = [
        	[
                'name' => 'Admin',
                'email' => 'admin@primakara.ac.id',
                'password' => Hash::make('adminprimakara'),
                'role_id' => 3
            ],
            [
                'name' => 'Koordinator',
                'email' => 'koordinator@primakara.ac.id',
                'password' => Hash::make('koorprimakara'),
                'role_id' => 2
            ],
            [
                'name' => 'Staff',
                'email' => 'staff@primakara.ac.id',
                'password' => Hash::make('staffprimakara'),
                'role_id' => 1
            ]
        ];

        Role::insert($roles);
        User::insert($fields);
    }
}