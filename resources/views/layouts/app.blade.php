<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Sistem Pemberian Keputusan | Primakara Beasiswa') }}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  @yield('css-libraries')

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dashboard-assets/css/style.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dashboard-assets/css/components.min.css') }}">
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->
</head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="{{ asset('dashboard-assets/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->name }}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="{{ route('logout') }}" 
                onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"
                class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>    
              <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
            </div>
          </li>
        </ul>
      </nav>

      {{-- MAIN MENU DIKIRI --}}
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{ route('home') }}">BEASISWA PPA</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="/">BPPA</a>
          </div>
          <ul class="sidebar-menu">
            <li><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-fire"></i> <span>Beranda</span></a></li> 
            <li class="menu-header">Data Mahasiswa</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Mahasiswa</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('mahasiswa.add') }}">Input Data</a></li>
                <li><a class="nav-link" href="{{ route('mahasiswa.index') }}">Lihat Mahasiswa</a></li>
              </ul>
            </li>
            <li class="menu-header">Sistem Pendukung Keputusan</li>
            <li><a class="nav-link" href="{{ route('nilai-alternatif.index') }}"><i class="far fa-user"></i> <span>Nilai Alternatif</span></a></li>
            <li><a class="nav-link" href="{{ route('hasil.index') }}"><i class="fas fa-file-alt"></i> <span>Proses Perhitungan</span></a></li>
            <li><a class="nav-link" href="{{ route('ranking.index') }}"><i class="fas fa-trophy"></i> <span>Peringkat Beasiswa</span></a></li>
            <li class="menu-header">Lainnya</li>
            {{-- <li><a class="nav-link" href="{{ route('seleksi-manual') }}"><i class="fas fa-file-alt"></i> <span>Seleksi Manual</span></a></li> --}}
            <li><a class="nav-link" href="{{ route('settings.index') }}"><i class="fas fa-pencil-ruler"></i> <span>Settings</span></a></li>
            <li><a class="nav-link" href="{{ route('persyaratan') }}"><i class="fas fa-file-alt"></i> <span>Persyaratan Beasiswa</span></a></li> 
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        @yield('content')
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2020 <div class="bullet"></div> Build by <a href="https://primakara.ac.id/" target="_blank">Primakara</a>
        </div>
        <div class="footer-right"></div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('dashboard-assets/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/popper.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->
  @yield('js-libraries')

  <!-- Page Specific JS File -->
  @yield('page-specific-js')
  
  <!-- Template JS File -->
  <script src="{{ asset('dashboard-assets/js/scripts.js') }}"></script>
  <script src="{{ asset('dashboard-assets/js/custom.js') }}"></script>
</body>
</html>