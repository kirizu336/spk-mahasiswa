<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Sistem Pemberian Keputusan | Primakara Beasiswa') }}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  @yield('css-libraries')

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dashboard-assets/css/style.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dashboard-assets/css/components.min.css') }}">
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->
</head>

<body>
  <div id="app">
    @yield('content')
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('dashboard-assets/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/popper.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dashboard-assets/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->
  @yield('js-libraries')

  <!-- Page Specific JS File -->
  @yield('page-specific-js')
  
  <!-- Template JS File -->
  <script src="{{ asset('dashboard-assets/js/scripts.js') }}"></script>
  <script src="{{ asset('dashboard-assets/js/custom.js') }}"></script>
</body>
</html>