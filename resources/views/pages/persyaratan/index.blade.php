@extends('layouts.app')

@section('css-libraries')

@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Persyaratan Beasiswa PPA Primakara</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Persyaratan Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <iframe src="{{ asset('laraview/#../dashboard-assets/documents/persyaratan.pdf') }}" width="100%" height="500px"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-libraries')

@endsection

@section('page-specific-js')

@endsection