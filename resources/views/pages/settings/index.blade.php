@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard-assets/modules/jquery-yearpicker/yearpicker.css') }}">
<style>
    .yearpicker-container{
		z-index: 99999;
	}
</style>
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <div class="section-header-back">
      <a href="features-settings.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
    </div>
    <h1>Settings</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    @if($errors->any())
    <div class="alert alert-danger alert-has-icon">
      <div class="alert-icon">
        <i class="far fa-lightbulb"></i>
      </div>
      <div class="alert-body">
        <div class="alert-title">Whoops! Something were wrong!</div>
        {{ $errors->first() }}
      </div>
    </div>
    @endif
    <h2 class="section-title">Settings</h2>
    <p class="section-lead">
      Pengaturan Website Primakara Beasiswa
    </p>

    <div id="output-status"></div>
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h4>Jump To</h4>
          </div>
          <div class="card-body">
            <ul class="nav nav-pills flex-column">
              <li class="nav-item"><a href="#" class="nav-link active">General</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <form id="setting-form" action="{{ route('settings.store') }}" method="POST">
          @csrf
          <div class="card" id="settings-card">
            <div class="card-header">
              <h4>Periode Beasiswa</h4>
            </div>
            <div class="card-body">
              <p class="text-muted">Pengaturan Umum yang mengatur periode beasiswa, jumlah kuota beasiswa dan bobot kriteria</p>
              <div class="form-group row align-items-center">
                <label for="site-title" class="form-control-label col-sm-3 text-md-right">Tahun Ajaran</label>
                <div class="col-sm-6 col-md-9">
                  @php
                    $periode_tahun_ajaran = explode('/', $currSetup->periode_tahun_ajaran);
                  @endphp
                  <input type="text" name="periode_tahun_ajaran_part_one" placeholder="Ex: 2019" value="{{ $periode_tahun_ajaran[0] }}" class="form-control yearpicker" style="width: auto;display: inline-block;" required>
                  -
                  <input type="text" name="periode_tahun_ajaran_part_two" placeholder="2020" value="{{ $periode_tahun_ajaran[1] }}" class="form-control yearpicker" style="width: auto;display: inline-block;" required>
                </div>
              </div>
              <div class="form-group row align-items-center">
                <label class="form-control-label col-sm-3 text-md-right">Kuota Beasiswa</label>
                <div class="col-sm-6 col-md-9">
                  <div class="input-group" style="width: 40%;">
                    <input id='kuota-beasiswa' type="number" name="kuota_beasiswa" class="form-control" style="display: inline-block;" value="{{ $currSetup->kuota_beasiswa }}" {{ $currSetup->kuota_beasiswa < 0 ? 'readonly' : '' }} required>
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        Orang
                      </div>
                    </div>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input id="beasiswa-unlimited" type="checkbox" name="kuota_beasiswa_unlimited" class="custom-control-input" {{ $currSetup->kuota_beasiswa == '-1' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="beasiswa-unlimited">Tidak Terbatas</label>
                  </div>
                </div>
              </div>
              <div class="form-group row align-items-center">
                <label class="form-control-label col-sm-3 text-md-right">Bobot C1.IPK</label>
                <div class="col-sm-6 col-md-9">
                  <div class="input-group" style="width: 40%;">
                    <input type="number" name="bobot_c1" step="any" class="form-control" style="display: inline-block;" min="0" value="{{ $currSetup->bobot_c1 }}" required>
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        %
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row align-items-center">
                <label class="form-control-label col-sm-3 text-md-right">Bobot C2.SKS</label>
                <div class="col-sm-6 col-md-9">
                  <div class="input-group" style="width: 40%;">
                    <input type="number" name="bobot_c2" step="any" class="form-control" style="display: inline-block;" min="0" value="{{ $currSetup->bobot_c2 }}" required>
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        %
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row align-items-center">
                <label class="form-control-label col-sm-3 text-md-right">Bobot C3.Poin TAK</label>
                <div class="col-sm-6 col-md-9">
                  <div class="input-group" style="width: 40%;">
                    <input type="number" name="bobot_c3" step="any" class="form-control" style="display: inline-block;" min="0" value="{{ $currSetup->bobot_c3 }}" required>
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        %
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row align-items-center">
                <label class="form-control-label col-sm-3 text-md-right">Bobot C4.Penghasilan Orang Tua</label>
                <div class="col-sm-6 col-md-9">
                  <div class="input-group" style="width: 40%;">
                    <input type="number" name="bobot_c4" step="any" class="form-control" style="display: inline-block;" min="0" value="{{ $currSetup->bobot_c4 }}" required>
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        %
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div style="display: flex;flex-direction: row;">
                <button id="btn-delete-all" type="button" style="align-self: flex-end;" class="btn btn-danger ml-auto">Hapus Data Tahun Ajaran</button>
              </div>
            </div>
            <div class="card-footer bg-whitesmoke text-md-right">
              <button type="submit" class="btn btn-success" id="save-btn">Simpan Perubahan</button>
              <button type="reset" class="btn btn-danger">Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
	    </div>
	</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/jquery-yearpicker/yearpicker.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endsection

@section('page-specific-js')
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', () => {
  $('.yearpicker').yearpicker();
  $('#beasiswa-unlimited').on('change', function() {
    if ($(this).is(':checked')) {
      $('#kuota-beasiswa').attr('readonly', '');
      $('#kuota-beasiswa').val('-1');
    }else{
      $('#kuota-beasiswa').removeAttr('readonly');
      $('#kuota-beasiswa').val(10);
    }
  });

  $('#btn-delete-all').on('click', function(e){
    e.preventDefault();

    showDelete();
  });

  function showDelete() {
    swal({
          title: "Confirmation",
          text: "Apa anda yakin ingin menghapus data seluruh mahasiswa tahun ajaran {{ $periode_tahun_ajaran[0] }} - {{ $periode_tahun_ajaran[1] }} ?!",
          icon: "warning",
          closeOnClickOutside: false,
          buttons: {
              cancel: {
                  text: "Cancel",
                  visible: true,
                  closeModal: true
              },
              confirm: {
                  text: "Iya saya yakin!",
                  visible: true,
                  closeModal: false
              }
          },
          dangerMode: true
      })
      .then((willPost) => {
          if (willPost) {
              $.ajaxSetup({
                  headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
              });
              $.ajax({
                  type: "POST",
                  url: "{{ route('mahasiswa.delete.all') }}",
                  success: function(){
                      swal({
                          title: "Operation Succeeded!",
                          text: "Data berhasil dihapus",
                          icon: "success"
                      })
                      .then(() => {
                          table.ajax.reload();
                      });
                  },
                  error: function(err) {
                      swal({
                          title: "Whoops! something were wrong!",
                          text: "please contact admin!",
                          icon: "error"
                      });
                      console.error(err);
                  },
                  cache: false,
                  contentType: false,
                  processData: false
              });
          }
      });
  }
});
</script>
@endsection