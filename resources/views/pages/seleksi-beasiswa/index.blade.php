@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Proses Perhitungan</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    @if($errors->any())
    <div class="alert alert-danger alert-has-icon">
      <div class="alert-icon">
        <i class="far fa-lightbulb"></i>
      </div>
      <div class="alert-body">
        <div class="alert-title">Whoops! Something were wrong!</div>
        {{ $errors->first() }}
      </div>
    </div>
    @endif
    <h2 class="section-title">Proses Perhitungan</h2>
    <p class="section-lead">
      Dalam halaman ini menampilkan hasil perbaikan bobot, hasil perhitungan skor dan hasil perhitungan vektor</b>
    </p>

    <div class="row" style="margin-bottom: 20px;">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Perbaikan Bobot</h4>
            <div class="card-header-action">
              <form action="{{ route('hasil.hitung') }}" method="POST">
                @csrf
                <input type="submit" value="Jalankan Perhitungan" class="btn btn-success">
              </form>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-skor">
                <thead>                                 
                  <tr>
                    <th class="text-center">W1</th>
                    <th class="text-center">W2</th>
                    <th class="text-center">W3</th>
                    <th class="text-center">W4</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!isset($c1))
                  <tr>
                    <td colspan="8" class="text-center">Lakukan hitung untuk melihat</td>
                  </tr>
                  @else
                  <tr>
                    <td class="text-center">{{ $c1 }}</td>
                    <td class="text-center">{{ $c2 }}</td>
                    <td class="text-center">{{ $c3 }}</td>
                    <td class="text-center">{{ $c4 }}</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Hasil Perhitungan Skor </h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped datatable" id="table-skor">
                <thead>                                 
                  <tr>
                    <th style="width: 10px" class="text-center">
                      #
                    </th>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th>C1</th>
                    <th>C2</th>
                    <th>C3</th>
                    <th>C4</th>
                    <th>Hasil Skor</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!isset($hasilSkor))
                  <tr>
                    <td colspan="8" class="text-center">Lakukan hitung untuk melihat</td>
                  </tr>
                  @else
                    @foreach($hasilSkor as $skor)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $skor->mahasiswa->nim }}</td>
                      <td>{{ $skor->mahasiswa->nama_lengkap }}</td>
                      <td>{{ $skor->c1_pow }}</td>
                      <td>{{ $skor->c2_pow }}</td>
                      <td>{{ $skor->c3_pow }}</td>
                      <td>{{ $skor->c4_pow }}</td>
                      <td>{{ $skor->total_pow }}</td>
                    </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Hasil Perhitungan Vektor</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped datatable" id="table-vektor">
                <thead>                                 
                  <tr>
                    <th style="width: 10px" class="text-center">
                      #
                    </th>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th>Hasil Vektor</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!isset($finalResult))
                  <tr>
                    <td colspan="8" class="text-center">Lakukan hitung untuk melihat</td>
                  </tr>
                  @else
                    @foreach($finalResult as $result)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $result->mahasiswa->nim }}</td>
                      <td>{{ $result->mahasiswa->nama_lengkap }}</td>
                      <td>{{ $result->total_pow }}</td>
                    </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var table = $('table.datatable').DataTable({
      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
      pageLength: 5
    });
  });
</script>
@endsection