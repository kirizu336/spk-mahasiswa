@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Proses Beasiswa</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Seleksi Manual</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Penerima Beasiswa</h2>
    <p class="section-lead">
      Seleksi Beasiswa. Kuota saat ini: <b>0</b>.
    </p>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Daftar Calon Penerima</h4>
            <div class="card-header-action">
              <a href="#" class="btn btn-success">Simpan</a>
              <a href="#" class="btn btn-danger">Batal</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-1">
                <thead>                                 
                  <tr>
                    <th style="width: 10px" class="text-center">
                      <div class="custom-checkbox custom-control">
                        <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                        <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                      </div>
                    </th>
                    <th>Nim</th>
                    <th style="width: 300px">Nama</th>
                    <th style="width: 40px">Jenis Kelamin</th>
                    <th style="width: 40px">Prodi</th>
                    <th style="width: 40px">Angkatan</th>
                    <th style="width: 40px">Status Menerima</th>
                    <th style="width: 50px">Tahun Ajaran</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @for($i=0; $i<5; $i++)                                 
                  <tr>
                    <td class="align-middle">
                      <div class="custom-checkbox custom-control">
                        <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-{{ $i + 1 }}">
                        <label for="checkbox-{{ $i + 1 }}" class="custom-control-label">&nbsp;</label>
                      </div>
                    </td>
                    <td class="align-middle">180802931</td>
                    <td class="align-middle">
                      Parta
                    </td>
                    <td class="align-middle">
                      Laki-laki
                    </td>
                    <td class="align-middle">IF</td>
                    <td class="align-middle">2019</td>
                    @if($i < 4)
                    <td class="align-middle">
                      <div class="badge badge-success">Belum Pernah</div>
                    </td>
                    @else
                    <td class="align-middle">
                      <div class="badge badge-danger">Sudah Pernah</div>
                    </td>
                    @endif
                    <td class="align-middle">2019/2020</td>
                    <td class="align-middle">
                      <a href="#" class="btn btn-primary trigger--fire-modal" id="modal-{{ $i + 1 }}">Detail</a>
                    </td>
                  </tr>
                  <div class="modal-part" id="modal-data-part-{{ $i + 1 }}">
                    <div class="form-group">
                      <label>Nomor Induk Mahasiswa</label>
                      <p>180802931</p>
                    </div>
                    <div class="form-group">
                      <label>Nama</label>
                      <p>Parta</p>
                    </div>
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <p>Laki-Laki</p>
                    </div>
                    <div class="form-group">
                      <label>Prodi</label>
                      <p>Teknik Informatika</p>
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <p>Denpasar</p>
                    </div>
                    <div class="form-group">
                      <label>IPK (C1)</label>
                      <p>nilai ipk disini</p>
                    </div>
                    <div class="form-group">
                      <label>Total SKS (C2)</label>
                      <p>nilai total sks disini</p>
                    </div>
                    <div class="form-group">
                      <label>Total Poin TAK (C3)</label>
                      <p>nilai total poin tak disini</p>
                    </div>
                    <div class="form-group">
                      <label>Penghasilan Orang Tua (C4)</label>
                      <p>penghasilan disini</p>
                    </div>
                    <div class="form-group">
                      <label>Semester (C5)</label>
                      <p>semester disini</p>
                    </div>
                    <div class="form-group">
                      <label>Angkatan</label>
                      <p>2019</p>
                    </div>
                    <div class="form-group">
                      <label>Tahun Ajaran</label>
                      <p>2019/2020</p>
                    </div>
                  </div>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('page-specific-js')
<script src="{{ asset('dashboard-assets/js/page/modules-datatables.js') }}?v=1"></script>
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', () => {
    @for($i=0; $i<5; $i++)
    $("#modal-{{ $i + 1 }}").fireModal({
      title: 'Detail Mahasiswa',
      body: $('#modal-data-part-{{ $i + 1 }}')
    });
    @endfor
  });
</script>
@endsection