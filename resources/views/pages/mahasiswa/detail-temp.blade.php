<script id="details-template" type="text/x-handlebars-template">
  <table class="table">
    <tr>
      <td>Nomor Induk Mahasiswa</td>
      <td>@{{ nim_mahasiswa }}</td>
    </tr>
    <tr>
      <td>Nama Lengkap</td>
      <td>@{{ mahasiswa.nama_lengkap }}</td>
    </tr>
    <tr>
      <td>Jenis Kelamin</td>
      <td>@{{ mahasiswa.jenis_kelamin }}</td>
    </tr>
    <tr>
      <td>Prodi</td>
      <td>@{{ mahasiswa.prodi }}</td>
    </tr>
    <tr>
      <td>Alamat Lengkap</td>
      <td>@{{ mahasiswa.alamat_lengkap }}</td>
    </tr>
    <tr>
      <td>IPK</td>
      <td>@{{ ipk }}</td>
    </tr>
    <tr>
      <td>Total SKS</td>
      <td>@{{ total_sks }}</td>
    </tr>
    <tr>
      <td>Total Point TAK</td>
      <td>@{{ total_point_tak }}</td>
    </tr>
    <tr>
      <td>Penghasilan Orang Tua</td>
      <td>@{{ penghasilan_orang_tua }}</td>
    </tr>
    <tr>
      <td>Semester</td>
      <td>@{{ semester }}</td>
    </tr>
    <tr>
      <td>Angkatan</td>
      <td>@{{ mahasiswa.angkatan }}</td>
    </tr>
    <tr>
      <td>Tahun Ajaran</td>
      <td>@{{ tahun_ajaran }}</td>
    </tr>
  </table>
</script>