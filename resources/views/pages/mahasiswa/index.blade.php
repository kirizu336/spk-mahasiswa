@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
<style type="text/css">
  .small-col{
    width: 10px !important;
  }
  .big-col{
    width: 100%;
  }
  .truncate {
    max-width: 55px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
</style>
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Daftar Mahasiswa</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Calon Penerima Beasiswa</h2>
    <p class="section-lead">
      Daftar calon penerima beasiswa PPA di STMIK Primakara
    </p>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Daftar Mahasiswa</h4>
            <div class="card-header-action">
            	<a href="{{ route('mahasiswa.add') }}" class="btn btn-danger">Tambah Baru</a>
            	<a href="{{ route('excel.import') }}" class="btn btn-success">Import Excel</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-mhs">
                <thead>                                 
                  <tr>
                    <th class="text-center">Nim</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">IPK</th>
                    <th class="text-center">Total SKS</th>
                    <th class="text-center">Total P.TAK</th>
                    <th class="text-center">P. Ortu</th>
                    <th class="small-col text-center">Semester</th>
                    <th class="small-col text-center">Angkatan</th>
                    <th class="text-center">Tahun Ajaran</th>
                    <th class="big-col text-center">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/handlebars/handlebars.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endsection

@section('page-specific-js')

@include('pages.mahasiswa.detail-temp')

<script type="text/javascript">
  $(document).ready(function() {
    var template = Handlebars.compile($("#details-template").html());

    var table = $('#table-mhs').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ route('mahasiswa.ajax') }}',
      columns: [
        {data: 'nim_mahasiswa', name: 'nim_mahasiswa'},
        {data: 'mahasiswa.nama_lengkap', name: 'mahasiswa.nama_lengkap', className: 'truncate'},
        {data: 'ipk', name: 'ipk'},
        {data: 'total_sks', name: 'total_sks'},
        {data: 'total_point_tak', name: 'total_point_tak'},
        {data: 'penghasilan_orang_tua', name: 'penghasilan_orang_tua'},
        {data: 'semester', name: 'semester', className: 'small-col text-center'},
        {data: 'mahasiswa.angkatan', name: 'mahasiswa.angkatan', className: 'small-col text-center'},
        {data: 'tahun_ajaran', name: 'tahun_ajaran'},
        {data: 'action', name: 'action', orderable: false, searchable: false, className: 'big-col text-center'}
      ],
      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
      pageLength: 5,
      autoWidth: false
    });

    // Add event listener for opening and closing details
    $('#table-mhs tbody').on('click', 'td > button.btn.btn-primary.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
        }
        else {
            // Open this row
            row.child( template(row.data()) ).show();
        }
    });


    function deleteMhs(deleteUrl){
      swal({
          title: "Confirmation",
          text: "Ketika menghapus mahasiswa maka akan menghapus seluruh data terkaitnya (nilai alternatif dan hasil ranking dari si mahasiswa). Apa anda yakin?",
          icon: "warning",
          closeOnClickOutside: false,
          buttons: {
              cancel: {
                  text: "Cancel",
                  visible: true,
                  closeModal: true
              },
              confirm: {
                  text: "Iya saya yakin!",
                  visible: true,
                  closeModal: false
              }
          },
          dangerMode: true
      })
      .then((willPost) => {
          if (willPost) {
              $.ajaxSetup({
                  headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
              });
              $.ajax({
                  type: "POST",
                  url: deleteUrl,
                  success: function(){
                      swal({
                          title: "Operation Succeeded!",
                          text: "Data berhasil dihapus",
                          icon: "success"
                      })
                      .then(() => {
                          table.ajax.reload();
                      });
                  },
                  error: function(err) {
                      var errors = err.responseJSON.errors;
                      swal({
                          title: "Whoops! something were wrong!",
                          text: "please contact admin!",
                          icon: "error"
                      });
                      console.error(err);
                  },
                  cache: false,
                  contentType: false,
                  processData: false
              });
          }
      });
    }

    $('#table-mhs tbody').on('click', 'td > button.btn.btn-danger.btn-delete[data-remote]', function(e) {
        e.preventDefault();
        var rowData = $(this).attr('data-remote');
        deleteMhs(rowData);
    });
  });
</script>
@endsection