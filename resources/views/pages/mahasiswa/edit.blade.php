@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/jquery-selectric/selectric.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard-assets/modules/jquery-yearpicker/yearpicker.css') }}">
<style type="text/css">
	.left-form{
		padding-right: 20px;
	}
	.right-form{
		padding-left: 20px;
	}
	.yearpicker-container{
		z-index: 99999;
	}

	@media screen and (max-width: 680px){
		.left-form,
		.right-form{
			padding-right: 0;
			padding-left: 0;
		}
	}
</style>
@endsection

@section('content')
<section class="section">
	<div class="section-header">
	    <h1>Edit Mahasiswa</h1>
	    <div class="section-header-breadcrumb">
	      <div class="breadcrumb-item">Edit Mahasiswa</div>
	    </div>
	</div>
	<div class="section-body">
		@if($errors->any())
		<div class="alert alert-danger alert-has-icon">
			<div class="alert-icon">
				<i class="far fa-lightbulb"></i>
			</div>
			<div class="alert-body">
				<div class="alert-title">Whoops! Something were wrong!</div>
				{{ $errors->first() }}
			</div>
		</div>
		@endif
		<h2 class="section-title">Ubah Data Mahasiswa</h2>
        <p class="section-lead">Pengisian formulir data diri lengkap mahasiswa Primakara</p>

        <form action="{{ route('mahasiswa.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
	        <div class="row">
	        	<div class="col-12">
	        		<div class="card">
	        		  <div class="card-header">
	        		  	<h4>Form Pengisian</h4>
	        		  	<div class="card-header-action">
	        		  		<input type="submit" class="btn btn-success" value="Simpan Perubahan">
	        		  		<input type="reset" class="btn btn-danger" value="Batal">
	        		  	</div>
	        		  </div>
	                  <div class="card-body">
	                    <div class="row">
	                    	<div class="col-xs-12 col-md-6 left-form">
	                    		<div class="form-group">
			                      <label>Nomor Induk Mahasiswa</label>
			                      <input type="number" class="form-control" placeholder="Nomor Induk Mahasiswa" name="nim" value="{{ $currMhs->nim }}" readonly required>
			                    </div>
			                    <div class="form-group">
			                      <label>Nama Lengkap</label>
			                      <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama_lengkap" value="{{ $currMhs->nama_lengkap }}" required>
			                    </div>
			                    <div class="form-group">
			                      <label>Jenis Kelamin</label>
			                      <select class="form-control selectric" name="jenis_kelamin" required>
			                      	<option value="L" {{ $currMhs->jenis_kelamin === 'L' ? 'selected' : '' }}>Laki-laki</option>
			                      	<option value="P" {{ $currMhs->jenis_kelamin === 'P' ? 'selected' : '' }}>Perempuan</option>
			                      </select>
			                    </div>
			                   	<div class="form-group">
			                      <label>Prodi</label>
			                      <select class="form-control selectric" name="prodi" required>
			                      	<option value="IF" {{ $currMhs->prodi === 'IF' ? 'selected' : '' }}>Teknik Informatika</option>
			                      	<option value="SI" {{ $currMhs->prodi === 'SI' ? 'seleected' : '' }}>Sistem Informasi</option>
			                      	<option value="SIA" {{ $currMhs->prodi === 'SIA' ? 'selected' : '' }}>Sistem Informasi Akuntansi</option>
			                      </select>
			                    </div>
			                    <div class="form-group">
			                      <label>Alamat Lengkap</label>
			                      <input type="text" class="form-control" placeholder="Alamat Lengkap" name="alamat_lengkap" value="{{ $currMhs->alamat_lengkap }}" required>
			                    </div>
			                    <div class="form-group">
			                      <label>IPK</label>
			                      <input type="number" class="form-control" placeholder="ex: 3.65" name="ipk" value="{{ $currMhs->history->first()->ipk }}" step="any" min="3.000" max="4.000" required>
			                    </div>
	                    	</div>
	                    	<div class="col-xs-12 col-md-6 right-form">
	                    		<div class="form-group">
			                      <label>Total SKS</label>
			                      <input type="number" class="form-control" min="8" placeholder="ex: 30" name="total_sks" value="{{ $currMhs->history->first()->total_sks }}" required>
			                    </div>
			                    <div class="form-group">
			                      <label>Total Point TAK</label>
			                      <input type="number" class="form-control" min="0" placeholder="ex: 30" name="total_point_tak" value="{{ $currMhs->history->first()->total_point_tak }}" required>
			                    </div>
			                    <div class="form-group">
			                      <label>Penghasilan Orang Tua</label>
			                      <input type="text" class="form-control currency" placeholder="Ex: 5.000.000" name="penghasilan_orang_tua" value="{{ $currMhs->history->first()->penghasilan_orang_tua }}" autocomplete="off" required>
			                    </div>
			                    <div class="form-group">
			                    	<label>Semester</label>
				                    <input type="number" class="form-control" placeholder="ex: 2" min="1" name="semester" value="{{ $currMhs->history->first()->semester }}" max="14" required>
			                    </div>
			                    <div class="form-group">
			                    	<label>Angkatan</label>
				                    <input type="text" class="form-control yearpicker" placeholder="ex: 2019" name="angkatan" value="{{ $currMhs->history->first()->angkatan }}" required>
			                    </div>
			                    <div class="form-group">
			                    	<label>Tahun Ajaran</label>
				                    <div class="col-12">
					                    @php
						                    $yearBefore = new DateTime('-1 year');
						                @endphp
						                <input type="text" name="tahun_ajaran_part_one" placeholder="Ex: 2019" value="{{ $yearBefore->format('Y') }}" value="{{ $currMhsTahunAjaran[0] }}" class="form-control" style="width: auto;display: inline-block;" readonly required>
						                <input type="text" name="tahun_ajaran_part_two" placeholder="2020" value="{{ date('Y')}}" value="{{ $currMhsTahunAjaran[1] }}" class="form-control" style="width: auto;display: inline-block;" readonly required>
						            </div>
						            <b>*Mengikuti Tahun Ajaran pada Options</b>
						        </div>
	                    	</div>
	                    </div>
	                  </div>
	                </div>
	        	</div>
	        </div>
    	</form>
	</div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/cleave-js/cleave.min.js')}}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-yearpicker/yearpicker.js') }}"></script>
@endsection

@section('page-specific-js')
<script src="{{ asset('dashboard-assets/js/page/forms-advanced-forms.js') }}"></script>
@endsection