@extends('layouts.app')

@section('css-libraries')
<style type="text/css">
	.left-form{
		padding-right: 20px;
	}
	.right-form{
		padding-left: 20px;
	}
	.yearpicker-container{
		z-index: 99999;
	}

	@media screen and (max-width: 680px){
		.left-form,
		.right-form{
			padding-right: 0;
			padding-left: 0;
		}
	}
</style>
@endsection

@section('content')
<section class="section">
	<div class="section-header">
	    <h1>Tambah Mahasiswa</h1>
	    <div class="section-header-breadcrumb">
	      <div class="breadcrumb-item">Import</div>
	    </div>
	</div>
	<div class="section-body">
		@if($errors->any())
		<div class="alert alert-danger alert-has-icon">
			<div class="alert-icon">
				<i class="far fa-lightbulb"></i>
			</div>
			<div class="alert-body">
				<div class="alert-title">Whoops! Something were wrong!</div>
				{{ $errors->first() }}
			</div>
		</div>
		@endif
		<h2 class="section-title">Import Calon Penerima Baru</h2>
        <p class="section-lead">Import calon penerima beasiswa</p>

        <form action="{{ route('excel.exec') }}" method="POST" enctype="multipart/form-data">
        	@csrf
	        <div class="row">
	        	<div class="col-12">
	        		<div class="card">
	        		  <div class="card-header">
	        		  	<h4>Import Data Mahasiswa</h4>
	        		  	<div class="card-header-action">
	        		  		<input type="submit" class="btn btn-success" value="Tambahkan">
	        		  		<input type="reset" class="btn btn-danger" value="Batal">
	        		  	</div>
	        		  </div>
	                  <div class="card-body">
	                    <div class="row">
	                    	<div class="col-xs-12 col-md-6 left-form">
	                    		<div class="form-group">
			                      <label>File Mahasiswa</label>
			                      <input type="file" name="file" accept=".xlsx,.xls,.csv" class="form-control" required>
			                    </div>
	                    	</div>
	                    </div>
	                  </div>
	                </div>
	        	</div>
	        </div>
    	</form>
	</div>
</section>
@endsection