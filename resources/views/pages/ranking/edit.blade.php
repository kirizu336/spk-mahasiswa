@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/jquery-selectric/selectric.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard-assets/modules/jquery-yearpicker/yearpicker.css') }}">
<style type="text/css">
	.left-form{
		padding-right: 20px;
	}
	.right-form{
		padding-left: 20px;
	}
	.yearpicker-container{
		z-index: 99999;
	}

	@media screen and (max-width: 680px){
		.left-form,
		.right-form{
			padding-right: 0;
			padding-left: 0;
		}
	}
</style>
@endsection

@section('content')
<section class="section">
	<div class="section-header">
	    <h1>Edit Status</h1>
	    <div class="section-header-breadcrumb">
	      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
	    </div>
	</div>
	<div class="section-body">
		@if($errors->any())
		<div class="alert alert-danger alert-has-icon">
			<div class="alert-icon">
				<i class="far fa-lightbulb"></i>
			</div>
			<div class="alert-body">
				<div class="alert-title">Whoops! Something were wrong!</div>
				{{ $errors->first() }}
			</div>
		</div>
		@endif
		<h2 class="section-title">Edit Status Mahasiswa</h2>
        <p class="section-lead">Status Mahasiswa</p>

        <form action="{{ route('ranking.edit.store', ['id' => $ranking->id]) }}" method="POST" enctype="multipart/form-data">
        	@csrf
	        <div class="row">
	        	<div class="col-12">
	        		<div class="card">
	        		  <div class="card-header">
	        		  	<h4>Data</h4>
	        		  	<div class="card-header-action">
	        		  		<input type="submit" class="btn btn-success" value="Simpan Perubahan">
	        		  		<input type="reset" class="btn btn-danger" value="Batal">
	        		  	</div>
	        		  </div>
	                  <div class="card-body">
	                    <div class="row">
	                    	<div class="col-xs-12 col-md-6 left-form">
			                    <div class="form-group">
			                      <label>Status</label>
			                      <select class="form-control selectric" name="status" required>
			                      	<option value="belum-pernah" {{ $ranking->mahasiswa->firstHistory->first()->status === 'belum-pernah' ? 'selected' : '' }}>Belum Pernah</option>
			                      	<option value="sudah-pernah" {{ $ranking->mahasiswa->firstHistory->first()->status === 'sudah-pernah' ? 'selected' : '' }}>Sudah Pernah</option>
			                      </select>
			                    </div>
			                   	
	                    	</div>
	                    	<div class="col-xs-12 col-md-6 right-form">
	                    		&nbsp;
	                    	</div>
	                    </div>
	                  </div>
	                </div>
	        	</div>
	        </div>
    	</form>
	</div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/cleave-js/cleave.min.js')}}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-yearpicker/yearpicker.js') }}"></script>
@endsection

@section('page-specific-js')
<script src="{{ asset('dashboard-assets/js/page/forms-advanced-forms.js') }}"></script>
@endsection