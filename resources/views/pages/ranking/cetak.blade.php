<!DOCTYPE html>
<html>
<head>
	<title>Beasiswa PPA Tahun Ajaran {{ $setup->periode_tahun_ajaran }}</title>
	<style type="text/css">
		#customers {
		  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		  border-collapse: collapse;
		  width: 100%;
		}

		#customers td, #customers th {
		  border: 1px solid #000;
		  padding: 8px;
		  text-align: center;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers th {
		  padding-top: 8px;
		  padding-bottom: 8px;
		  text-align: center;
		}
	</style>
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h2>Beasiswa PPA Tahun Ajaran {{ $setup->periode_tahun_ajaran }}</h2>
	</center>

	<table id="customers">
		<thead>
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Nim</th>
				<th class="text-center">Nama Lengkap</th>
				<th class="text-center">Jenis Kelamin</th>
				<th class="text-center">Prodi</th>
				<th class="text-center">Alamat</th>
				<th class="text-center">IPK</th>
				<th class="text-center">Total SKS</th>
				<th class="text-center">Total TAK</th>
				<th class="text-center">PO</th>
				<th class="text-center">Semester</th>
				<th class="text-center">Angkatan</th>
				<th class="text-center">Thn Ajaran</th>
				<th class="text-center">Nilai Vektor</th>
			</tr>
		</thead>
		<tbody>
			@foreach($rankings as $ranking)
			<tr>
				<td class="text-center">{{ $loop->iteration }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->nim }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->nama_lengkap }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->jenis_kelamin === 'L' ? 'Laki-laki' : 'Perempuan' }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->prodi }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->alamat_lengkap }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->firstHistory->first()->ipk }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->firstHistory->first()->total_sks }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->firstHistory->first()->total_point_tak }}</td>
				<td class="text-center">{{ number_format($ranking->mahasiswa->firstHistory->first()->penghasilan_orang_tua, 2)}}</td>
				<td class="text-center">{{ $ranking->mahasiswa->firstHistory->first()->semester }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->angkatan }}</td>
				<td class="text-center">{{ $ranking->mahasiswa->firstHistory->first()->tahun_ajaran }}</td>
				<td class="text-center">{{ $ranking->total_pow }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>