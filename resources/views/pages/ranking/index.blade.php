@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
<style type="text/css">
  .dataTables_length{
    display: inline-block;
    margin-right: 20px;
  }
</style>
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Ranking</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Peringkat Beasiswa</h2>
    <p class="section-lead">
      Peringkat calon Penerima Beasiswa PPA Primakara
    </p>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Daftar Mahasiswa</h4>
            <div class="card-header-action">
              <a href="{{ route('ranking.cetak.all') }}" target="_blank" class="btn btn-danger">Cetak PDF</a>
              <a id="btn-cetak" href="{{ route('ranking.cetak') }}" target="_blank" class="btn btn-success">Cetak Penerima</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-mhs">
                <thead>                                 
                  <tr>
                    <th>Nim</th>
                    <th style="width: 100px">Nama</th>
                    <th style="width: 40px">Prodi</th>
                    <th style="width: 40px">Angkatan</th>
                    <th style="width: 40px">Tahun Ajaran</th>
                    <th style="width: 40px">Status Menerima</th>
                    <th style="width: 90px">Perhitungan Vektor</th>
                    <th style="width: 300px" class="text-center">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/handlebars/handlebars.min.js') }}"></script>
@endsection

@section('page-specific-js')

@include('pages.ranking.detail-temp')

<script type="text/javascript">
  $(document).ready(function() {
    var template = Handlebars.compile($("#details-template").html());

    var table = $('#table-mhs').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: "{{ route('ranking.ajax') }}",
        data: function(d){
          d.status = $('#table-mhs_wrapper > .row > .col-sm-12.col-md-6:first > select').val();
        }
      },
      columns: [
        {data: 'mahasiswa.nim', name: 'mahasiswa.nim'},
        {data: 'mahasiswa.nama_lengkap', name: 'mahasiswa.nama_lengkap'},
        {data: 'mahasiswa.prodi', name: 'mahasiswa.prodi'},
        {data: 'mahasiswa.angkatan', name: 'mahasiswa.angkatan'},
        {data: 'tahun_ajaran', name: 'tahun_ajaran'},
        {
          classname: 'align-middle',
          data: 'status',
          name: 'status'
        },
        {data: 'total_pow', name: 'total_pow'},
        {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
      ],
      order: [
        [6, 'desc']
      ],
      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
      pageLength: 5,
      searching: false
    });

    // Add event listener for opening and closing details
    $('#table-mhs tbody').on('click', 'td > button.btn.btn-primary.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
        }
        else {
            // Open this row
            row.child( template(row.data()) ).show();
        }
    });

    table.on('draw', function() {
      if ($('#table-mhs_wrapper > .row > .col-sm-12.col-md-6:first > select').length < 1) {
        //creating filter
        var filterInput = $("<select id='#filter-rank' class='form-control' style='width:50%;display:inline-block;'></select>");

        //options
        var optionOne = $("<option value='' hidden selected></option>").text('Pilih Status Menerima');
        filterInput.append(optionOne);

        var optionTwo = $("<option value='Belum Pernah'></option>").text('Belum Pernah');
        filterInput.append(optionTwo);

        var optionThree = $("<option value='Sudah Pernah'></option>").text('Sudah Pernah');
        filterInput.append(optionThree);

        var optionFour = $("<option value='all'></option>").text('Munculkan Semua');
        filterInput.append(optionFour);
         

        $('#table-mhs_wrapper > .row > .col-sm-12.col-md-6:first').append(filterInput);
      }

      $('#table-mhs_wrapper > .row > .col-sm-12.col-md-6:first > select').on('change', function(){
        table.draw();

        //update cetak href
        $('#btn-cetak').attr('href', urlGenerator('status', $(this).val().toLowerCase().replace(' ', '-')));
      });

      function urlGenerator (key, value) {
          let temp = '(?<='+`${key}`+'=)[^&]+'
          let reg = new RegExp(temp,'g');
          var url = "{{route('ranking.cetak')}}?status=belum-pernah";
          // change the value for given key
          return url.replace(reg, value);
      }
    });
  });
</script>
@endsection