@extends('layouts.app')

@section('content')
<section class="section">
  <div class="section-header">
    <h1><marquee scrolldelay="120">SELAMAT DATANG DI SISTEM PENDUKUNG KEPUTUSAN PEMBERIAN BEASISWA PPA STMIK PRIMAKARA</marquee></h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Beasiswa PPA STMIK Primakara</h2>
    <div class="alert alert-info alert-has-icon">
      <div class="alert-icon">
        <i class="far fa-lightbulb"></i>
      </div>
      <div class="alert-body">
        <div class="alert-title">SPK Pemberian Beasiswa PPA STMIK Primakara</div>
        <p>Sistem pendukung keputusan pemberian beasiswa PPA ini menggunakan metode <b>Weighted Product</b> dengan kriteria IPK, Total SKS, Total Poin TAK dan Penghasilan Orang Tua.</p>
      </div>
    </div>
  </div>
</section>
@endsection
