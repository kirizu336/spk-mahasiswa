@extends('layouts.app')

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Nilai Alternatif</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item">Beasiswa PPA Primakara</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Nilai Alternatif</h2>
    <p class="section-lead">
      Nilai Alternatif diperoleh dari hasil normalisasi data kriteria dari masing-masing mahasiswa
    </p>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Daftar Mahasiswa</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-mhs">
                <thead>                                 
                  <tr>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th style="width: 10px" class="text-center">C1</th>
                    <th style="width: 10px" class="text-center">C2</th>
                    <th style="width: 10px" class="text-center">C3</th>  
                    <th style="width: 10px" class="text-center">C4</th>
                    <th>Tahun Ajaran</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-libraries')
<script src="{{ asset('dashboard-assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/handlebars/handlebars.min.js') }}"></script>
@endsection

@section('page-specific-js')

@include('pages.kriteria.detail-temp')

<script type="text/javascript">
  var template = Handlebars.compile($("#details-template").html());

    var table = $('#table-mhs').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ route('nilai-alternatif.ajax') }}',
      columns: [
        {data: 'nim_mahasiswa', name: 'nim_mahasiswa'},
        {data: 'mahasiswa.nama_lengkap', name: 'mahasiswa.nama_lengkap'},
        {data: 'nilai_c1', name: 'nilai_c1'},
        {data: 'nilai_c2', name: 'nilai_c2'},
        {data: 'nilai_c3', name: 'nilai_c3'},
        {data: 'nilai_c4', name: 'nilai_c4'},
        {data: 'tahun_ajaran', name: 'tahun_ajaran'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
      ],
      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
      pageLength: 5
    });

    // Add event listener for opening and closing details
    $('#table-mhs tbody').on('click', 'td > button.btn.btn-primary.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
        }
        else {
            // Open this row
            row.child( template(row.data()) ).show();
        }
    });
</script>
@endsection