<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/imports')->group(function() {
	Route::post('/', 'ExcelController@import')->name('excel.exec');
	Route::get('/', 'ExcelController@importMahasiswa')->name('excel.import');
});

Route::prefix('/settings')->group(function() {
	Route::post('/', 'AppSetupController@updateSetting')->name('settings.store');
	Route::get('/', 'AppSetupController@indexAppSetup')->name('settings.index');
});

// Route::get('/seleksi-manual', function() {
// 	return view('pages.seleksi-beasiswa.add');
// })->name('seleksi-manual');

Route::prefix('/ranking')->group(function() {
	Route::post('/edit/{id}', 'RankingController@storeRanking')->name('ranking.edit.store');
	Route::get('/edit/{id}', 'RankingController@editRanking')->name('ranking.edit');
	Route::get('/cetak-all', 'RankingController@cetakAll')->name('ranking.cetak.all');
	Route::get('/cetak', 'RankingController@cetakRanking')->name('ranking.cetak');
	Route::get('/ajaxranking', 'RankingController@ajaxRanking')->name('ranking.ajax');
	Route::get('/', 'RankingController@indexRanking')->name('ranking.index');
});

Route::prefix('/hasil')->group(function() {
	Route::post('/hitung', 'SeleksiController@spk')->name('hasil.hitung');
	Route::get('/', 'SeleksiController@main')->name('hasil.index');
});

Route::prefix('/nilai-alternatif')->group(function() {
	Route::get('/ajaxkriteria', 'KriteriaController@ajaxkriteria')->name('nilai-alternatif.ajax');
	Route::get('/', 'KriteriaController@indexKriteria')->name('nilai-alternatif.index');
});

Route::prefix('/mahasiswa')->group(function() {
	Route::post('/delete-all', 'MahasiswaController@deleteAllMahasiswaByTahunAjaran')
	->name('mahasiswa.delete.all');
	Route::post('/delete/{nim}', 'MahasiswaController@deleteMahasiswa')->name('mahasiswa.delete');
	Route::get('/edit/{nim}', 'MahasiswaController@editMahasiswa')->name('mahasiswa.edit');
	Route::post('/tambah-baru', 'MahasiswaController@storeMahasiswa')->name('mahasiswa.store');
	Route::get('/tambah-baru', 'MahasiswaController@createMahasiswa')->name('mahasiswa.add');
	Route::get('/ajaxmahasiswa', 'MahasiswaController@datatableIndexMahasiswa')->name('mahasiswa.ajax');
	Route::get('/', 'MahasiswaController@indexMahasiswa')->name('mahasiswa.index');
});

Route::get('/persyaratan-beasiswa-ppa', function() {
	return view('pages.persyaratan.index');
})->name('persyaratan');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return redirect()->to(route('login'));
});
